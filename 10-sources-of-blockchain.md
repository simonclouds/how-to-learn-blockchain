10 个区块链资讯网站收集
===


[巴比特](http://www.8btc.com/)

[bitcoin.org](https://bitcoin.org/zh_CN/)

[bitcoinwiki](https://en.bitcoin.it/wiki/Main_Page)

[blockchain-blog](https://blog.blockchain.com/)

[剑桥-区块链](https://www.cambridge-blockchain.com/)

[cointelegraph](https://cointelegraph.com/)

[ccn](https://www.ccn.com)

[比特币交易资讯](https://www.btctrade.com/bitcoin/)

[比特币中文资讯](https://cn.bitcoin.com/)

[火球财经](http://huoqiucj.com/)