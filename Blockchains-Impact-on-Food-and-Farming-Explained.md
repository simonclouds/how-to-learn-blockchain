[Blockchain\'s Impact on Food and Farming, Explained]()
===

区块链对食品业和农业的冲击
===


## 1. Is it possible to track where food comes from?
> 能追踪食物的来源吗？

Several companies have launched services allowing shoppers to see a product’s journey from farm to fork, but they often depend on retailers agreeing to be transparent.
> 许多公司已上线了服务以让商店能追踪产品从农场到餐桌的完整流程，但是这通常依赖于零售商同意信息透明。

When you pop into a store to buy fresh fruit, vegetables or meat, it’s common for the packaging to reveal which country it is from. Some upmarket brands go further by offering stories about the farm and the conditions where the food was cultivated.
> 当你走进一家商店准备买点新鲜水果/蔬菜/肉类，一般而言可以从食材的包装上获知其来源于哪个国家。一些高端的品牌则更进一步，会展示食材所在的农场和食物的生长耕作环境。

Tracking an item step-by-step through the manufacturing process can be hard — and, sometimes, even manufacturers and retailers themselves aren’t sure about a product’s journey.
> 想要一步一步跟踪产品的整个加工过程是非常困难的，而且有时候，连生产商或零售商自己也搞不清楚产品的整个流程。

This is part of the reason why recent food safety scares have been so widespread: it’s been difficult — nearly impossible — to track where problems begin. However, integrating blockchain into the food chain could mean issues are detected instantly.
> 这也是最近对于食品安全的恐慌迅速蔓延的部分原因：想要搞清楚问题的源头太困难了，几乎不可能。然而，若将区块链集成到食品加工产业链中将能迅速定位问题所在。

## 2. Food safety scares! Remind me?
> 食品安全恐慌！提醒我？
There have been several in recent years… how long have you got?
> 近年来已经多起...你有多久...?

In 2018, a deadly E. coli outbreak was connected to romaine lettuce that was grown in Arizona. The outbreak affected 35 states across the U.S., with five people dying and a total of almost 200 cases reported.
> 2018年，一场与亚利桑那州种植的生菜有关的大肠杆菌感染爆发，蔓延到美国35个州，5人死亡，近200例病例报道。

In 2013, a horsemeat scandal gripped Europe — and products advertised as beef were actually found to contain… yep, you guessed it, horse. It made its way into some of the continent’s biggest supermarkets. They blamed their suppliers, who in turn blamed their suppliers. The furore shook confidence and left some people refusing to buy meat all together.
> 2013年，一起马肉丑闻笼罩欧洲 —— 实际上作为牛肉产品的广告中居然包含，是的，你没猜错，马肉。想方设法地进入了一些洲的最大的超市销售。这些超市责备他们的供应商，而供应商由责备供应商的供应商。群众丧失了对肉类的信心还集体抵制购买肉类了。

Blockchain could play a big role in clamping down on food fraud because every component in a finished product would become easier to identify — speeding up recalls and also allowing consumers to find information they can trust about an item, seconds after picking it up off the shelf.
> 区块链在打压食品业诈骗过程中可扮演重要角色，因为最终成品的每个组成部分都可以轻松标记 —— 这将加快溯果寻因的过程并且能让客户在从货架拿起商品的秒秒钟内就能了解到这个商品得以被信任的信息。

## 3. How would that work? Any examples?
> 其如何工作的？举个例子？

Smart agriculture solutions — which boost productivity and address food demand — are thriving, and it is predicted that this industry could be worth up to $26.76 billion by 2020.
> 智能农业解决方案 —— 促进生产力+致力于事物需求 —— 正在兴起，预计到2020年该产业将达到$267.6亿美元的市场。

Farmers who are already using blockchain describe it as a “game changer, just like the internet.” For example, one meat company says the seemingly insignificant statistics blockchain provides, such as when pigs arrive at a factory, can have a massive effect on the final product they deliver to customers. They show blockchain results in a “detailed passport” where consumers can be assured that the meat they are consuming met strict hygiene and well-being standards — with any issues arising in the production process being identified in as little as 30 seconds.
> 区块链的尝鲜农民形容区块链是来修改游戏规则，就像当年互联网做的事一样。例如，一家肉品公司表示，区块链所提供的那些看似无关紧要的统计数据（如生猪是何时到达宰杀工厂的）将对交付给用户的最终成品产生重大影响。他们将区块链的结果展示在一个“详细的护照”上，在这个上面消费者得以确认他们所消费的肉品是符合严格的卫生和健康标准 —— 任何生产过程中出现的问题在短短30秒内就能确认。

Meanwhile, the government of Kerala is planning to introduce blockchain in the grocery supply process, with the hope of ensuring that the system will be used whenever food is being delivered to stores across the country. It is hoped this will help deliver products to millions of people on a daily basis more efficiently, as well as provide a form of “crop insurance” to ensure that farmers can be compensated quickly whenever unforeseen circumstances affect their yield.
> 与此同时，Kerala 政府计划将区块链引进到食品杂货店的供货流程中，希望确保每当食品运送到商店时使用该系统。区块链被寄予厚望它将帮助让数百万人的商品供需运转更高效，同时也提供一种“农作物保险”的金融产品可以让那些遭遇未知状况导致生产损失的农民快速得到赔偿。

Certification of fruit and vegetables could also be enhanced through blockchain — ensuring that information isn’t lost and streamlining the manpower needed to confirm a product’s provenance. Every shipment of fruit and vegetables is accompanied by paper certificates showing where the food has come from, validating its quality and declaring it free of disease. In Belgium, work has begun to digitize some of these certificates so they are placed on a blockchain instead.
> 蔬菜和水果的鉴定通过区块链也得以增强 —— 确保信息不会丢失，并可以简化认证蔬菜水果来源所需的人力。每批水果和蔬菜都附带着纸质证书，证书上用于说明其来源，校验数量以及确保没有收到灾病感染。在 Belgium，某些证书已经开始数字化了，因此他们就干脆用区块链取而代之。

## 4. Food safety’s great, but what about food prices? They’re constantly rising.
> 食品安全很重要，食品价格有如何呢？其正不断上升。

Blockchain could eliminate paper-based processes and cut costs, with these savings passed on to you.
> 区块链将消除基于纸张的流程并降低成本，最终将使作为消费者的你从中收益。

Getting rid of middlemen would minimize transaction fees, and decentralization would also make it easier for smaller farms to compete with larger corporations.
> 剔除中间商后可将运输费最小化，同时去中心化将使得小农场可以与大集团进行竞争。

For example, blockchain concepts such as PavoCoin are entering the fray — giving smaller farmers access to attainable financial services, such as the ability to pre-sell crops via smart contracts, helping them to improve the quality and quantity of their harvests, and providing consumers with greater amounts of information about the food they are buying. The company says, farmers in Stockton and in Dixon, California, have recently started using the system. A third installation is planned in Merced soon.
> 例如，像PavoCoin这样的区块链概念正加入竞争 —— 让小农场也可以获得金融服务，比如通过智能合约预售农产品，帮助他们提高产量提升品质，以及为消费者提供更多关于食品的信息。该公司表示，在Stockton、Dixon和California的农民，已经开始使用这个系统了，Merced 都计划第三次安装了。

Optimizing farming would empower farmers of all scales with the information, resources and security they need to have higher yields, more lucrative crops. This would drive prices down for the average consumer and increase accessibility to higher quality goods. Higher yields also pump more money into the agricultural ecosystem, increasing the availability of farmed goods.
> 优化农业将为大大小小的农民附能，他们需要的信息、资源和安全，获得更高的产量以及种植经济效益更高的农作物。这将使得普通消费者的购买价格降低同时还能增加更高品质商品的可及性(买到质优价廉的商品)。更高的产量将为农业的整个生态系统注入更多的资金，产品的供应又得以提升。

## 5. But wouldn’t farmers be out of pocket from lower prices?
> 难道农民不会因为低价导致破产么？

On the contrary, smart contracts could ensure they are paid fairly for their hard work without delay — and smaller farms would have a larger market for selling their fresh produce.
> 恰恰相反，智能合约将使得农民的辛勤劳作得以公平的报酬，同时小农场将可能在一个更大的时长中销售他们的新鲜农产品。

Blockchain empowerment has the potential to shift farmers, who do the hardest work, from being price takers to price setters, forcing downstream cost reductions.
> 区块链将为农民赋能，将农民的身份从价格接受者转变为价格制定者，从而迫使下游成本降低。

Blockchain would also allow premium brands to stand out from the crowd — and justify why their free range or organic produce is worth the extra money. This is because the provenance of their goods can be easily traced, giving consumers confidence that they are getting added value from a high-end product.
> 区块链还可以让优质品牌脱颖而出 - 并证明为什么他们的自由放养或有机产品值得额外价格。 这是因为他们的商品的来源可以很容易追踪，让消费者相信他们从高端产品中获得了附加价值。

The agricultural sector would also find it easier to figure out how much their crops are worth by comparing the money being offered by a distributor to the sums paid to their rivals in earlier purchases, giving all farmers the opportunity to earn the money they deserve.
> 农业部门还可以通过比较经销商提供的资金与早期购买中支付给竞争对手的资金来更容易地计算出他们的作物价值多少，让所有农民有机会获得他们应得的收入。

## 6. Is there any way to get fresh food on store shelves faster?
> 有没有什么方法可以更快地从货架上获得新鲜食物？

Inefficiencies in the supply chain can often mean delays before produce is ready for sale, but automated processes through blockchain could speed things along nicely.
> 供应链中效率低下常常意味着产品销售就绪之前的延迟，但是基于区块链的自动化处理可以完美解决这个问题。

There could also be less chance of food going to waste because agricultural businesses and retailers would be able to gauge the demand for certain products and adjust supply accordingly. This spare capacity can then be devoted to other crops — eliminating food waste.
> 由于农业企业和零售商能够衡量对某些产品的需求并相应地调整供应，因此食品浪费的可能性也会降低。 这种剩余产能可以用于其他作物 —— 以此消除食物浪费。

Blockchain’s ability to tackle food waste, and the power that smart contracts can have in ensuring farmers get paid fairly and don’t go out of business, could help the world cater to rising levels of demand — helping eradicate food safety issues, poverty and political instability.
> 区块链解决食物浪费的能力以及智能合约在确保农民公平获得报酬和避免破产的能力，可以帮助世界满足不断增长的需求水平 —— 帮助消除食品安全问题，贫困和不稳定的政治因素。
