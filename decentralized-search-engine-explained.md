
Decentralized Search Engine Explained
===

> 去中心化搜索引擎阐释

## 1. What is a decentralized search engine?
> 去中心化搜索引擎是什么

A decentralized search engine makes use of distributed ledger technology.
> 去中心化搜索引擎使用分布式账本技术。

Using Blockchain gives you more privacy when it comes to searching online for the products and services that you love.
> 使用区块链让你在网上搜索喜爱的产品或服务时得到更多的隐私保护。

Instead of major search platforms deciding what is, what is not relevant to you, and basing their ad campaigns on a plethora of wide ranging data they silently collected, you decide what information you want to share.
> 比起主流搜索平台（它们决定哪些是与你相关的内容；它们悄悄地收集太多个人信息用于其广告战争），去中心化搜索引擎可以让你自己决定你想要分享哪些私人信息。

In return, you receive targeted ads that are of specific interested to you.
> 作为回馈，你将且仅将收到你真正感兴趣的广告。

Apart from giving users back the ownership of their personal information, it’s also beneficial to businesses. They are not forced to target people that big online advertising platforms tell them to target.
> 暂且不说将私密信息的所有权归还给用户自己，这同样对商家有很大好处。他们不用在迫不得已关注那些在线大广告平台想让他们关注的用户。


They only advertise to users who are already searching for their product and therefore have a relative degree of interest in it. This narrow-focus advertising can drastically improve conversion rates and increase sales.
> 他们只需要对搜索过自己产品或真的对其非常感兴趣的用户推送广告。这种精准的广告模式可以极大地提高转化率和销售额。

## 2. How does it work?
> 去中心化搜索引擎是如何工作的？

It functions like an ordinary search engine.
> 功能使用起来和常规搜索引擎别无二致。

It provides a platform for you to search the Internet. Relevant information about your searches is stored on the Blockchain. By continually browsing and interacting online through a Blockchain-based search engine, you are building up an online identity and buyer persona.
> 同样提供一个平台供你从互联网搜索信息。与你搜索切实相关的信息将会存储到区块链上。当你通过基于区块链的搜索引擎浏览信息和在线交互的过程中，你也同时建立自己的在线身份和买家角色。

This is already happening through conventional search engines, however, with a decentralized platform, you are in control of how your information is used and with whom it is shared. Your data is encrypted with a privacy key and stays secure and anonymous unless you decide otherwise.
> 其实，在传统搜索引擎中如上情形已经有实现了，然而，在有了去中心化加持的搜索平台，你能掌控你的个人私人信息如何使用以及与谁分享。你的数据经过私钥加密后（除非有你的许可）将变得非常安全和匿名化。

You have the option to make this information available to specific retailers by entering into a smart contract directly with the advertisers. This cuts out advertising middlemen who often bombard Internet users with meaningless pop-ups and sponsored ads that cost businesses millions in advertising costs.
> 您可以选择通过直接与广告商签订智能合约，将此信息提供给特定零售商。这削减了广告中间商，他们经常用无意义的弹出窗口和赞助广告轰炸互联网用户，这些广告使企业花费了数百万美元的广告费用。

In addition, users can also be compensated for each relevant search, transferred directly from the advertiser to the user. One such an example is BitClave’s Active Search Ecosystem (BASE) which will compensate users for relevant searches with their own digital currency, CAT (Consumer Activity Tokens). The currency can then be used for online purchases at accepting retailers.
> 另外，用户将能在每次有关联的搜索中直接获得来自广告主的补贴。有一个活生生的例子就是 BASE（BitClave 主动搜索生态系统），将在每一次相关的搜索中补贴用户，用的是其系统内自己的数字货币 CAT（用户活跃代币），在一些接受此币结算的商家可以直接用于消费。

The main benefit of such an ecosystem is that it allows users to potentially profit from their online searches and receive information about products they are truly interested in.
> 这样的一个生态系统最突出的好处在于其可以让用户从现实搜索中潜在的获益，且搜索到的产品信息都是他们真正感兴趣的。

In turn, businesses advertise directly to a user’s need. Shoppers are more likely to make a purchase at this point, which will drastically increase the average conversion rate of two percent that organizations pay billions of dollars to achieve.
> 作为反馈，商家的广告恰如其分地击中用户的需求。商店此时更容易完成一笔交易，这将显著提升 2% 的平均转化率，而这是机构要花费数十亿美元才能达到的效果。

## 3. Why do we need decentralized search engines?
> 我们为什么需要去中心化搜索引擎？

The online advertising sphere is ruled by a small number of giant corporations, including Google, Amazon and Facebook. 
> 在线广告领域仅仅受到几家巨无霸公司的左右：谷歌/亚马逊/脸书。

These big corporations basically dictate to businesses how they should run their advertising campaigns if they want to stay competitive.
> 这几家巨无霸基本上就决定了如果企业想要保持竞争力那么他们的广告战应该如何打。

What we might not always realize is that, whenever we browse the Internet, through search engines, social media or big retailers, a lot of our personal information is tracked and logged in some shape or form.
> 我们通常无法意识到无论何时我们浏览互联网或通过何种渠道如搜索引擎，社交媒介，电商巨头等，太多我们的个人信息通过一些表单什么的就被跟踪和记录了。

This information is then used in various ad campaigns that businesses pay huge sums of money for to be part of, hoping that they might get a piece of the billion-dollar online consumer industry. These campaigns are translated into the invasive pop-up ads, and mostly irrelevant, sponsored ads that litter our everyday online experience.
> 紧接着这些信息就被用于各种各样的广告活动，企业为此付出了巨额资金以期他们能从在线消费产业分得一杯数十亿美元羹。然后这些广告活动就转变成了侵入式弹出框广告，几乎不相关的赞助广告，然后我们每日的上网体验就跟吃了苍蝇一样。

The industry is therefore in need of disruption.
> 这个产业需要一场变革。

What a decentralized search engine, like BitClave, does is put the power back into the hands of everyday consumers and the businesses from which they buy.
> 去中心化搜索引擎，比如 BitClave ，要做的事就是将权力交回给寻常用户以及花钱购买用户信息的这些的企业。

It creates an environment where you only receive advertising from organizations that interest you and businesses only pay to advertise to people that are looking for their product.
> 这将开创新的生态环境：你只会收到你真正感兴趣的组织的广告推送，而企业只需向正在寻找其产品的用户投放广告和付费。

In simple terms, you get the opportunity to make money by receiving highly relevant offers while you browse the Internet.
> 简而言之，从今往后你将能在浏览互联网时接受高相关的广告推送，并因此获得一定收益。

For businesses, it means a significant reduction in advertising costs by cutting out the middlemen (ad campaigns), while at the same time achieving higher revenues through increased conversion rates as a result of the laser-focused advertising.
> 对企业而言，砍掉广告中间商后，支出将大大节省；同时由于精准的广告投放提升了转化率也使得营收显著提升。

## 4. How can it be applied in the real world?
> 去中心化搜索引擎在现实世界里如何落地？

Say you are looking at stocking your summer closet.
> 

While you are searching for the latest trends, the Blockchain-based application records information about the parameters within which you are searching.
> 当你搜索最新趋势时，基于区块链的应用将记录有关你想搜索的东西的参数信息。

This allows merchants to tailor their offers based on these parameters (i.e. summer clothes), making the advertising highly targeted and timely.
> 这允许商家基于这些参数（如夏季服装）定制他们的产品广告，使得广告精准且高效。

While you are searching, you are not bombarded with vague advertising that doesn’t concern you at that point in time, such as buying a new house or car, or a pick of the best restaurants in town.
> 当你在搜索时，将不再会被如新房新车广告或本地最好的餐厅推荐广告这样的与当前的需求八竿子打不着的广告狂轰滥炸。

At best, this irrelevant advertising only serves as a distraction from your current priority, which is to buy clothes.
> 充其量，这些无关痛痒的广告只会分散你当前目的——买衣服——的优先级。

However, if your priority is in fact to buy a new car or house, again the platform will ensure you only receive relevant offers based on your budget and other criteria you are conducting your search in.
> 即使你当前的优先级就是想要买一辆车或者一栋房子，平台也会确保你收到相关的广告，基于你的预算或者其他你正在搜索的其他标准的广告。

If you decide you want to find out a bit more about a specific offer and click on it, you receive compensation for your troubles.
> 如果你想要了解更多一些关于特定广告的信息并点击时，你将获得一定的报酬。

The use cases are nearly limitless and can be applied to any product or service we are interested in and searching for online. All the while getting compensated for the targeted offers that you receive.
> 应用场景可以说是毫无限制的，可以应用于我们所感兴趣的任何线上产品或服务。只要收到这样的精准的广告都能立即获得一定的补贴。
